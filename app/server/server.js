const app = require('./src/app');

const { PORT } = process.env;

app.listen(PORT, () => {
  console.log('App started');
  console.log(`http://localhost:${PORT}`);
});
