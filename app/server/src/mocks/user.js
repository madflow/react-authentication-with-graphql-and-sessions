const usersData = [
  {
    id: '17218820-68b2-4d36-b43f-bf283e14854f',
    email: 'carl@example.com',
    fullname: 'Carlo Carlito',
    password: 'carlo',
  },
];

async function findOne(email, password) {
  return new Promise((resolve, reject) => {
    const userExists = usersData.filter((user) => {
      return user.email === email && user.password === password;
    });
    Array.isArray(userExists) === true ? resolve(userExists.shift()) : reject(new Error('User does not exist'));
  });
}

async function findById(id) {
  return new Promise((resolve, reject) => {
    const userExists = usersData.filter((user) => {
      return user.id = id;
    });
    Array.isArray(userExists === true) ? resolve(userExists.shift()) : reject(new Error('User does not exist'));
  });
}

module.exports = { findById, findOne };
