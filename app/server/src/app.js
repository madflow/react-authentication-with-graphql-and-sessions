const express = require('express');
const passport = require('passport');
const session = require('express-session');
const { GraphQLLocalStrategy, buildContext } = require('graphql-passport');
const { ApolloServer } = require('apollo-server-express');
const typeDefs = require('./typeDefs');
const resolvers = require('./resolvers');
const User = require('./mocks/user');

/** App */
const app = express();
app.use(
  session({
    secret: 'BAD SECRET',
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize());
app.use(passport.session());

/** Passport */

passport.use(
  new GraphQLLocalStrategy(async (email, password, done) => {
    const matchingUser = await User.findOne(email, password);
    const error = matchingUser ? null : new Error('no matching user');
    done(error, matchingUser);
  })
);

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  User.findById(id, function (err, user) {
    done(err, user);
  });
});

/** Graphql Setup */

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req, res }) => buildContext({ req, res }),
});

apolloServer.applyMiddleware({ app });

module.exports = app;
