const { gql } = require('apollo-server-express');

const typeDefs = gql`
  type AuthPayload {
    user: User
  }

  type User {
    id: ID!
    email: String!
    fullname: String!
    password: String!
  }

  type Query {
    currentUser: User
    hostname: String
  }

  type Mutation {
    login(email: String!, password: String!): AuthPayload
    logout: Boolean
  }
`;

module.exports = typeDefs;
