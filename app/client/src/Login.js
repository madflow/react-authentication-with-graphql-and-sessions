import React from 'react';
import { Form, Field } from 'react-final-form';

export default function Login() {
  const onSubmit = async (values) => {
    setTimeout(() => {
      window.alert(JSON.stringify(values, 0, 2));
    }, 2000);
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <h2>Simple Default Input</h2>
          <div>
            <label>First Name</label>
            <Field
              name="firstName"
              component="input"
              placeholder="First Name"
            />
          </div>
          <button type="submit">Submit</button>
        </form>
      )}
    />
  );
}
