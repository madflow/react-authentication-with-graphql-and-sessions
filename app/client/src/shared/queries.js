import { gql } from '@apollo/client';

export const fragments = {
  userFields: gql`
    fragment UserFields on User {
      id
      email
      fullname
    }
  `,
};

export const LOGIN = gql`
  mutation Login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      user {
        ...userFields
      }
    }
  }
  ${fragments.userFields}
`;

export const CURRENT_USER = gql`
  query currentUser {
    currentUser {
      ...userFields
    }
  }
  ${fragments.userFields}
`;
