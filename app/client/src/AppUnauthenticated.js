import React from 'react';
import Login from './Login';

export default function AppUnauthenticated() {
  return (
    <React.Fragment>
      <h1>Not logged in</h1>

      <Login />
    </React.Fragment>
  );
}
