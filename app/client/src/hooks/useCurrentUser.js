import { useQuery } from '@apollo/client';
import { CURRENT_USER } from '../shared/queries';

export const useCurrentUser = () => {
  const { data } = useQuery(CURRENT_USER);
  return data;
};
