import React from 'react';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import { useCurrentUser } from './hooks/useCurrentUser';
import AppAuthenticated from './AppAuthenticated';
import AppUnauthenticated from './AppUnauthenticated';

const client = new ApolloClient({
  uri: '/graphql',
  cache: new InMemoryCache(),
});

function CurrentApp() {
  let isAuthenticated;
  const currentUser = useCurrentUser();
  if(currentUser && currentUser.data && currentUser.data.currentUser.id) {
    isAuthenticated = true;
  } else {
    isAuthenticated = false;
  }
  return isAuthenticated === true ? <AppAuthenticated /> : <AppUnauthenticated />;
}

export default function App() {
  return (
    <ApolloProvider client={client}>
      <CurrentApp></CurrentApp>
    </ApolloProvider>
  );
}
