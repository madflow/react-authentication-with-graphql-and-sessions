# react-authentication-with-graphql-and-sessions


## Development

* Node.JS 12
* YARN (classic)
* Clone the repository

```
yarn
yarn run start
```

## Client

* Create React App

## Server

* Express
* Passport

## Development

### Monorepo

* https://classic.yarnpkg.com/en/docs/workspaces/

### Authentication with Express, Graphql and Session

* https://jkettmann.com/authentication-and-authorization-with-graphql-and-passport/
* https://github.com/jkettmann/graphql-passport

### React Authentication

* https://medium.com/better-programming/building-basic-react-authentication-e20a574d5e71